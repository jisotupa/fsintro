(* 

F#

- staticly typed functional language with object oriented capabilities
- based on OCaml with influences from C#, Python, Haskell, Scala and Erlang
- current version: 4.0 (VS 2015)
- open source
- runs on .NET CLR, Mono
- Visual F#: the proprietary, closed source tooling in Visual Studio
- "Anything you can do in C#"
    
*) 

//////////////////////////////////////////////////////////////////////////////////////////////
// Bindings
//////////////////////////////////////////////////////////////////////////////////////////////
let x = 1
x = 2
//x <- 2

// No multiple definitions:
//let x = 2

// You can have code within definitions:
let foo =
    let y = 1
    let z = 2
    y + z

// Names can have whitespace in them
let ``name with spaces in it`` = "A bit hard to use from C#!"

// Defining a function
let add a b = a + b

add 1. 2.

// A function with no parameters, returning "unit"
let noParams () = printfn "This function has no params"
noParams ()

// Recursive functions
let rec factorial n =
    match n with
    | 0 -> 1
    | 1 -> 1
    | a when a < 0 -> 0
    | _ -> n * factorial (n - 1)

factorial 4

//////////////////////////////////////////////////////////////////////////////////////////////
// Expressions
//////////////////////////////////////////////////////////////////////////////////////////////

if 1 = 0 then true else false

for i = 0 to 1 do printfn "%d" i
for c in "blah" do printf "%c" c

open System
open System.Text

let sb = StringBuilder("Hello, F#")
let obj = sb :> Object
let mayFail = obj :?> System.Text.StringBuilder

new Object() :?> System.Text.StringBuilder

true && false
true || false
1 = 2
1 <> 2















//////////////////////////////////////////////////////////////////////////////////////////////
// Types
//////////////////////////////////////////////////////////////////////////////////////////////

// Strings

"foo \t \r\n"

@"
    SELECT N'""foo""'
    FROM FOO
"

"""<xml><element attribute="foo" /></xml>"""

"foo" = "foo"
"abc" < "foo"
"ab" < "abc"










// Objects
open System
let obj1 = new Object()
let obj2 = Object()























// Immutable linked lists
let linkedList = [1;2;3]
let linkedList2 =
    [
      0
      5
      6
    ]

linkedList = [1; 2; 3]
linkedList < linkedList2

0 :: linkedList

linkedList.Head
linkedList.[1]
linkedList.Tail
//linkedList.[1] <- 42










// Arrays
let array1 = [| 1;2;3 |]
let array2 =
    [|
        4
        5
        6
    |]

let til20 = [|1..20|]
let oddsTil20 = [| 1..2..22 |]

// Structural equality:
array1 = [|1;2;3|]

array1.[1]
array1.[1] <- 42
array1.[1]

array1 = [|1;2;3|]










// Normal .NET dictionary
open System.Collections.Generic

let dictionary = Dictionary<string, string>()
dictionary.["foo"] <- "mutability is bad for you!"
dictionary





















// Tuples
let tuple2 = (1, "foo")
let tuple3 = (1, "foo", true)

// Structural equality:
tuple2 = (1, "foo")
tuple2 > (1, "bar")

// tuple2 = tuple3

fst tuple2
snd tuple2

// Pattern matching tuples
let a, b = tuple2
let _, c, _ = tuple3



















// Immutable dictionary
let map = Map([(1,"foo"); (2, "bar")])
map.[1]
map.[0]
map.TryFind 0
map.TryFind 2

let map2 = map.Add(3, "baz")
map

// Also dictionaries have structural equality:
map = map2
map = Map([(1,"foo"); (2,"bar")])


















// Type aliases
type DimensionName = string

let dim : DimensionName = "Product"
let dim2 = "Product"
    
dim = dim2
















// Record types
type Entity =
    { Id: int
      Desc: string
      Dimension: DimensionName }

let product1 =
    { Id = 1
      Desc = "foo"
      Dimension = "Product" }

// Immutable
//product1.Desc <- "Foo"

// Structural equality
product1 = { Id = 1; Desc = "foo"; Dimension = "Product" }

// Creating a new record from existing record:
product1 = { product1 with Desc = "foo 2" }











// Discriminated unions
type CommandLineOption =
    | Verbose
    | Database of string
    | Server of string

[ Verbose; Server "SQL-SD1"; Database "Testing_JussiS" ]

type Temperature =
    | Kelvin of float
    | Celsius of float
    | Fahrenheit of float
    member x.ToKelvin () =
        match x with
        | Celsius c -> Kelvin (c + 273.15)
        | Fahrenheit f -> Kelvin ((f + 479.67) * 5. / 9.)
        | Kelvin _ -> x
    member x.ToCelsius () =
        match x with
        | Celsius _ -> x
        | Fahrenheit f -> Kelvin ((f - 32.0) * 5. / 9.)
        | Kelvin k -> Celsius (k - 273.15)
    override x.ToString () =
        match x with
        | Kelvin v -> sprintf "%.2f °K." v
        | Celsius v -> sprintf "%.2f °C." v
        | Fahrenheit v -> sprintf "%.2f °F." v 

let absoluteZero = Kelvin 0.0
let freezing = Celsius 0.
let fahrenheit = Fahrenheit 451.

absoluteZero.ToCelsius().ToString ()
freezing.ToKelvin().ToString()
fahrenheit.ToKelvin().ToString()








// Recursive discriminated unions
type Person = { FirstName: string; LastName: string }

type Employee =
    | Worker of Person
    | Agent of Person
    | Manager of Person * Employee list

let _007 =
    Agent { FirstName = "James"
            LastName = "Bond" }

let moneypenny =
    Worker { FirstName = "Eve"
             LastName = "Moneypenny" }

let m =
    Manager ({ FirstName = "Miles"
               LastName = "Messervy" },
             [_007; moneypenny])

let hasLicense employee =
    match employee with
    | Agent _ -> true
    | Manager (_, subordinates) ->
        printfn "Boss detected with %d direct subordinates!" (subordinates |> List.length)
        false
    | _ -> false

hasLicense _007
hasLicense m
hasLicense moneypenny









open System

// Interfaces
type IFoo =
    abstract member Bar : unit -> Person
    abstract member Baz : Person -> unit
    abstract member Rex : Employee list -> Employee 

// Classes
type Foo (person : Person) =
    
    let justSomeFunction () = ()

    interface IFoo with
        member t.Bar() = justSomeFunction(); person
        member t.Baz(person) = printfn "Person: %O" person
        member t.Rex(employees) = Manager (person, employees)
        
    interface IDisposable with
        member t.Dispose() = ()

    member t.Bar() = (t :> IFoo).Bar()
    member t.Blah(a, b) = printfn "Blahblah %s %s" a b

let queen = new Foo({ FirstName = "Elizabeth"; LastName = "Windsor" }) :> IFoo

queen.Bar()

// Only explicit interface implementations in F#:
queen.Rex([m])

// This won't work, because all IFoo impls do not have to implement IDispose
// (queen :> IDisposable).Dispose()

let queen2 = queen :?> Foo
queen2.Blah("foo", "bar")

// This works, because Foo implements IDisposable
(queen2 :> IDisposable).Dispose()

//////////////////////////////////////////////
// Functions
//////////////////////////////////////////////

// great type inference
// -> no need declare types all the time

// no early returns,
// everything is an expression

// every function has a return value
// and exactly one argument
// -----> Wat?














let add a b = a + b

// Pipe operator -> readable code
add 1 (add (add 3 1) 2)

3 |> add 1 |> add 2 |> add 1

// But don't get too clever with it:
1 |> add <| 2




let inc n = n + 1
inc 2

let inc' = add 1
inc' 3

// Reading function signatures:
// functions map from one domain to another
add
(|>)



// Creating your own operators
let (@!&) x f = f x

(@!&)

1 @!& add 1
1 |> add 1


 











// Currying

let add_v1 a b = a + b

let add_v2 a = fun b -> a + b

let add_v3 = fun a -> fun b -> a + b

let inc'' = add_v3 1
inc'' 2
















// Partial application

let add1 = add_v1 1

add1 2
add1 3

// Using operators like normal functions

(*) 2 3
(*) 2

let mul2 = (*) 2
mul2 3

[1;2] |> List.map ((*) 2)
      |> List.map mul2











// Function composition

let mul4 = mul2 >> mul2

mul4 1

let incMul2Add = inc >> mul2 >> add
incMul2Add 1 3

















// Option types
open System

let parseToInt str = Int32.Parse(str)

"0" |> parseToInt
"1" |> parseToInt

let toBool i = i <> 0

0 |> toBool
1 |> toBool
2 |> toBool

let parseToBool = parseToInt >> toBool

"0" |> parseToBool
"1" |> parseToBool

// In idiomatic C#, you lie and cheat all the time
// In idiomatic F#, you should not lie but do what you promise. So what happens here?
"foo" |> parseToBool

// Solution: change the mapped domains!
let parseToInt' str =
    match Int32.TryParse(str) with
    | false, _ -> None
    | true, i -> Some i

"foo" |> parseToInt'
"1" |> parseToInt'

let parseToBool' = parseToInt' >> (Option.map toBool)

"0" |> parseToBool'
"1" |> parseToBool'
"2" |> parseToBool'
"foo" |> parseToBool'
"" |> parseToBool'
null |> parseToBool'

// Short hand pattern matching with guards:
let print = function
    | Some b when b -> printfn "Set to true"
    | Some b -> printfn "Set to %b" b
    | None -> printfn "Could not parse"

"foo" |> parseToBool' |> print
"0" |> parseToBool' |> print
"1" |> parseToBool' |> print
















// Handling sequences:

let isEven a = a % 2 = 0

// Eager list handling:
let linkedList' = [1;2;3]
linkedList' |> List.reduce (fun a b -> a + b)
linkedList' |> List.reduce (+)
linkedList' |> List.sum

[1;2;3;4] |> List.reduce (*)

// Eager array handling:
let array1' = [|1;2;3|]

array1' |> Array.map ((*) 3)
        |> Array.filter isEven

array1' |> Array.sum

// Lazy, i.e. IEnumerable:
linkedList' |> Seq.map mul2
            |> Seq.reduce (+)

let infiniteRandomSequence max =
    let rand = Random()
    Seq.initInfinite (fun _ -> rand.Next(max))

10 |> infiniteRandomSequence
   |> Seq.where isEven
   |> Seq.take 20
   |> Seq.toList

// Computation expressions

// Creating sequencies

let randomSequence max =
    let rand = Random()
    seq { for i in 1..100 -> rand.Next(max) }

randomSequence 10
|> Seq.where isEven
|> Seq.take 5
|> Seq.toList

// Query expressions
#r "FSharp.Data.TypeProviders"
#r "System.Data.Linq"

open System
open Microsoft.FSharp.Data.TypeProviders

type schema =
    SqlDataConnection<"Data Source=sql-sd1; Initial Catalog=Testing_JussiS; Integrated Security=SSPI">

let db = schema.GetDataContext() 

// compile time type generation: strongly typed, intellisense works etc.
let q =
    query {
        for setting in db.Conf_Misc do
        where (setting.Explanation = null)
        select (setting.MemberName)
    }

q |> Seq.iter Console.WriteLine